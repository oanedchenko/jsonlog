package jsonlog

import (
	"encoding/json"
	"log"
	"os"
	"bufio"
	"strings"
	"text/template"
	"time"
	"flag"
	"fmt"
)

func main() {
	stat, _ := os.Stdin.Stat()
	if (stat.Mode() & os.ModeCharDevice) != 0 {
		fmt.Println(`Util to convert json log to plane format.

Usage examples:

* read whole log with less:
jsonlog < booking-v2_soa4.log | less

* read only errors:
jsonlog -x < booking-v2_soa4.log | less
`)
		return
	}

	x := flag.Bool("x", false, "exceptions only")
	flag.Parse()

	tpl, err := template.New("log-line").Parse(
`{{.time}}|{{.level}}|{{.thread}}|{{.message}}{{if .thrown}}
{{.thrown.name}}: {{.thrown.message}}{{range .thrown.extendedStackTrace}}
	at {{.class}}.{{.method}}({{.file}}:{{.line}}){{end}}{{end}}{{if .thrown.cause}}
Caused by: {{.thrown.cause.name}}: {{.thrown.cause.message}}{{range .thrown.cause.extendedStackTrace}}
	at {{.class}}.{{.method}}({{.file}}:{{.line}}){{end}}{{end}}{{if .thrown.cause.cause}}
Caused by: {{.thrown.cause.cause.name}}: {{.thrown.cause.cause.message}}{{range .thrown.cause.cause.extendedStackTrace}}
	at {{.class}}.{{.method}}({{.file}}:{{.line}}){{end}}{{end}}{{if .thrown.cause.cause.cause}}
Caused by: {{.thrown.cause.cause.cause.name}}: {{.thrown.cause.cause.cause.message}}{{range .thrown.cause.cause.cause.extendedStackTrace}}
	at {{.class}}.{{.method}}({{.file}}:{{.line}}){{end}}{{end}}{{if .thrown.cause.cause.cause.cause}}
Caused by: {{.thrown.cause.cause.cause.cause.name}}: {{.thrown.cause.cause.cause.cause.message}}{{range .thrown.cause.cause.cause.cause.extendedStackTrace}}
	at {{.class}}.{{.method}}({{.file}}:{{.line}}){{end}}{{end}}{{if .thrown.cause.cause.cause.cause.cause}}
Caused by: {{.thrown.cause.cause.cause.cause.cause.name}}: {{.thrown.cause.cause.cause.cause.cause.message}}{{range .thrown.cause.cause.cause.cause.cause.extendedStackTrace}}
	at {{.class}}.{{.method}}({{.file}}:{{.line}}){{end}}{{end}}{{if .thrown.cause.cause.cause.cause.cause.cause}}
Caused by: {{.thrown.cause.cause.cause.cause.cause.cause.name}}: {{.thrown.cause.cause.cause.cause.cause.cause.message}}{{range .thrown.cause.cause.cause.cause.cause.cause.extendedStackTrace}}
	at {{.class}}.{{.method}}({{.file}}:{{.line}}){{end}}{{end}}
`)
	if err != nil {
		log.Panic(err)
	}

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		line := scanner.Text()
		var v map[string]interface{}
		dec := json.NewDecoder(strings.NewReader(line))
		if err := dec.Decode(&v); err != nil {
			log.Println(err)
			return
		}
		if !*x || v["thrown"] != nil {
			// convert time:
			timeMillis := int64(v["timeMillis"].(float64))
			t := time.Unix(0, timeMillis*int64(time.Millisecond))
			v["time"] = t.String()

			tpl.Execute(os.Stdout, v)
		}
	}
}