# Overview

Simple util to convert java logs in json format to plane text to make them understandable for IDEs.

Another feature is filter only entries with error stacktrace.

# Usage

* simple:
`jsonlog < json-log-file.log | less`

* error filtering:
`jsonlog < json-log-file.log -x| less`